#ifndef INIREADER_H
#define INIREADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>

using namespace std;

namespace Ini
{
	//--------------------------------------------------------------------

	const int INI_READ = 1;
	const int INI_WRITE = 2;

	//--------------------------------------------------------------------

	class IniItem
	{
	private:
		string value;
	public:
		IniItem():value(""){}
		IniItem(string Value):value(Value){}
		IniItem(char *Value):value(Value){}
		IniItem(int Value)
		{
			stringstream ss;
			ss<<Value;
			value = ss.str();
		}
		IniItem(double Value)
		{
			stringstream ss;
			ss<<Value;
			value = ss.str();
		}

		operator string()
		{
			return value;
		}
		operator int()
		{
			return atoi(value.c_str());
		}
		operator double()
		{
			return atof(value.c_str());
		}
	};

	//--------------------------------------------------------------------

	class IniCategory
	{
	private:
		map<string, IniItem > Items;
	public:
		IniItem& operator[](string name)
		{
			return Items[name];
		}
		friend class IniReader;
	};

	//--------------------------------------------------------------------

	class IniReader
	{
	private:
		map<string, IniCategory > Category;
		string filename;
		int mode;

		void ReadIni();
		void WriteIni();
	public:
		IniReader():filename(""),mode(0){};
		IniReader(string FileName,int Mode):filename(FileName),mode(Mode)
		{
			if(mode & INI_READ)
				ReadIni();
		}
		void Open(string FileName,int Mode)
		{
			Category.clear();
			filename = FileName;
			mode = Mode;
			if(mode & INI_READ)
				ReadIni();
		}
		IniCategory& operator[](string name)
		{
			return Category[name];
		}
		void Close()
		{
			if(mode & INI_WRITE)
				WriteIni();
		}
		~IniReader()
		{
			if(mode & INI_WRITE)
				WriteIni();
		}
	};
}

#endif