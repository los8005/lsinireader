IniReader - simply C++ INI Read/Writer. Simple example:

	#include <iostream>
	#include "inireader.h"

	using namespace std;
	using namespace Ini;

	int main(int argc, char *argv[])
	{
		IniReader ir("test.ini",INI_READ|INI_WRITE);

		ir["Cat 1"]["val1"] = 5;
		ir["Cat 1"]["val2"] = 5.6;
		ir["Cat 1"]["val3"] = "Hahahah";

		ir.Close();
		
		return 0;
	}