#include "inireader.h"

using namespace Ini;

void IniReader::ReadIni()
{
	ifstream ifs(filename.c_str(),ifstream::in);

	string cur_cat = "";
	while(!ifs.eof())
	{
		string str;
		getline(ifs,str);
		
		if(str.find("[") == string::npos && str.find("=") == string::npos)
		{
			continue;
		}
		else if(str[0] == '[')
		{
			cur_cat = str.substr(1,str.find("]")-1);
		}
		else
		{
			Category[cur_cat][str.substr(0,str.find("="))] = str.substr(str.find("=")+1,str.length());
		}
	}

	ifs.close();
}

void IniReader::WriteIni()
{
	ofstream ofs(filename.c_str(),ofstream::out);

	for(map<string, IniCategory >::iterator it = Category.begin();it != Category.end();it++)
	{
		ofs<<"["<<it->first<<"]"<<endl;
		for(map<string, IniItem >::iterator pit = it->second.Items.begin();pit != it->second.Items.end();pit++)
		{
			ofs<<pit->first<<"="<<(string)pit->second<<endl;
		}
	}

	ofs.close();
}